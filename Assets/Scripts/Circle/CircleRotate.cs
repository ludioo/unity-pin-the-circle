﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleRotate : MonoBehaviour
{
    [SerializeField]
    private float rotateSpeed = 50f;

    private bool canRotate;
    private float angle;

    private void Awake()
    {
        canRotate = true;
        StartCoroutine(ChangeRotation());
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(canRotate)
        {
            RotateTheCircle();
        }
    }

    void RotateTheCircle()
    {
        angle = transform.rotation.eulerAngles.z;
        angle += rotateSpeed * Time.deltaTime;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));

    }

    IEnumerator ChangeRotation()
    {
        yield return new WaitForSeconds(2f);
        if(Random.Range(0,2)>0)
        {
            rotateSpeed = -Random.Range(30, 70);

        }else
        {
            rotateSpeed = Random.Range(30, 70);
        }

         StartCoroutine(ChangeRotation());
    }
}
