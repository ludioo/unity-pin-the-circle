﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeedleHead : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D target)
    {
        if(target.tag == "Needle Head")
        {
            Time.timeScale = 0f;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
