﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeedleMovement : MonoBehaviour
{
    [SerializeField]
    private GameObject needleBody;

    private bool canFireNeedle;
    private bool touchedCircle;

    private float forceY = 50f; //Speed
    private Rigidbody2D myBody;

    private void Awake()
    {
        Initialize();
    }

    void Initialize()
    {
        needleBody.SetActive(false);
        myBody = GetComponent<Rigidbody2D>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(canFireNeedle)
        {
            myBody.velocity = new Vector2(0, forceY);
        }
    }

    public void FireTheNeedle()
    {
        needleBody.SetActive(true);
        myBody.isKinematic = false;
        canFireNeedle = true;
    }

    private void OnTriggerEnter2D(Collider2D target)
    {
          
        if (touchedCircle)
            return;

        if(target.tag == "Circle")
        {
            Debug.Log("Touched");
            canFireNeedle = false;
            touchedCircle = true;
            myBody.isKinematic = true;
            myBody.velocity = new Vector2(0, 0);
            gameObject.transform.SetParent(target.transform);
            if(ScoreManager.instance != null)
            {
                ScoreManager.instance.SetScore();
            }
            
            
        }
    }
}
